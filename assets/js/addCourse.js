let createForm = document.querySelector("#createCourse");

createForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let name = document.querySelector("#courseName").value;	
	let description = document.querySelector("#courseDescription").value;
	let price = document.querySelector("#coursePrice").value;
	// let image = document.querySelector("#courseImage").value;

	let adminCheck = window.localStorage.getItem('isAdmin'); 
	console.log(adminCheck)
	if (adminCheck == "true"){
		fetch("https://fathomless-cove-53036.herokuapp.com/api/courses", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
                name: name,
                description: description,
                price: price
			})
		})
		.then(res => res.json())
		.then(data => {
					console.log(data)
					if (data === true) {
						alert("New course successfully registered.")
	                    // localStorage.setItem("courseId", data._id)
	                    // note, bawal na ID din sa taas, kasi magiging same sila as yung kay USER na id din si key.
	                    // localStorage.setItem("isActive", data.isActive)
	                    // localStorage.setItem("createdOn", data.createdOn)
						// window.location.replace("./login.html")
					} else {
						alert("Something went wrong in adding the course.")
					}
					})
	} else {
		alert("Oops. You're not allowed to do that.")
	}	

	// for the COURSES page, upon "Create Course", this is what's going to happen:
	// let adminButton = document.querySelector("#adminButton").innerHTML = "IT WORKED? HE HE HE"

})