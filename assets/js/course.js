// REMAINING: CHANGE MO SI PLACEHOLDER PARA MADIDIRECT SIYA SA SPECIFIC NA GUSTO MONG COURSE!!!!!!! 
// IN THIS CASE, SI 5f8d71878d82283c5007014a
// GET IT THROUGH LOCAL STORAGEEEE!!!!
// BAKA KAY ADDCOURSEJS PA LANG MAGSSTORE KA NA NG LOCAL STORAGEEEE!!!!


// let wishlist = document.getElementById('wishlist');

// wishlist.addEventListener('click', function (){
// 	let wishlisted = wishlist.innerHTML = '<i class="far fa-heart spaceIcon"></i> Wishlisted!'
// })

function reloadIt() {
    if (window.location.href.substr(-2) !== "?r") {
        window.location = window.location.href + "?r";
    }
}

reloadIt();
// reloadIt();

let courseId = localStorage.getItem("courseId")
let isAdmin = window.localStorage.getItem("isAdmin")

window.addEventListener("load", (e) => {
		let imageNo = window.localStorage.getItem("imageNo")
		console.log(imageNo)

		// then similarly, just call on imageNo para i backtick + dikit kay image source na scr = course+imageNo = course1, etc

		let courseId = localStorage.getItem("courseId")

		fetch(`https://fathomless-cove-53036.herokuapp.com/api/courses/${courseId} `, {
			method: "GET",
			headers: ({
				"Content-Type": "application/json",
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			console.log(data._id)

			let courseId = data._id

			let courseName = document.querySelector("#courseName").innerHTML = data.name;
			let coursePrice = document.querySelector("#coursePrice").innerHTML = data.price;
			let introDescription = document.querySelector("#introDescription").innerHTML = data.description; 
			let imageDiv = document.querySelector("#imageDiv").innerHTML = 

				`
				<img src="../assets/images/course${imageNo}.jpg" class="card-img" alt="...">
				`

			//  -- Start of IF ADMIN


			if (isAdmin == "true") {
				let enrolleeHeader = document.querySelector("#enrolleeHeader").innerHTML = "Enrollees"

					console.log(data.enrollees)
					console.log(data.enrollees.length)

					if (data.enrollees.length <= 0) {
						let enrollees = document.querySelector("#enrollees").innerHTML = 
							`
								<h6 class = "text-center" > No enrollees yet. </h6>
							`
					}
					// START OF FOR LOOPS
						for(let i = 0; i < data.enrollees.length; i++){

							fetch(`https://fathomless-cove-53036.herokuapp.com/api/users/${data.enrollees[i].userId} `, {
								method: "GET",
								headers: ({
									"Content-Type": "application/json",
								})
							})
							.then(res => res.json())
							.then(data => {
								console.log(data)
								// console.log(data.firstName)
								// console.log(data.lastName)

								let fullname = `${data.firstName} ${data.lastName}`
								console.log(fullname)
								console.log(`Enrollee ${i+1}`)

								localStorage.setItem(`Enrollee ${i+1}`, fullname)
								// reloadIt();

							})

							let enrollees = document.querySelector("#enrollees").innerHTML += 
							`
								<h6 class = "text-center" > ${window.localStorage.getItem(`Enrollee ${i+1}`)} </h6>
							`

						}
						// END OF FOR LOOPS			
			}

			// END OF IF ADMIN

			// START OF ENROLL
			let enrollContainer = document.querySelector("#enrollContainer")
			enrollContainer.addEventListener("click", (e)=> {
				reloadIt();
				
				// e.preventDefault();

				let userId = window.localStorage.getItem('id')
				let isAdmin = window.localStorage.getItem('isAdmin')

				if (isAdmin != "true" && (userId != null || userId != undefined)) {

				fetch("https://fathomless-cove-53036.herokuapp.com/api/users/enroll", {
		            method: "POST",
		            headers: {
		                "Content-Type": "application/json"
		            },
		            body: JSON.stringify({
		                userId: userId,
		                courseId: courseId
		            })
		        })
		        .then(res => {
		            return res.json()
		        })
		        .then(data => {
		            console.log(data)
		            if(data == true){
		            	alert("Congratulations! You have been enrolled.")
		            	window.location.href="./profile.html"
		            } else {
		                alert("Something went wrong. Enrollment unsuccessful.")
		            }
		        })

				} else if(isAdmin == "true"){
					alert("You are an admin.")
				} else {
					alert("Please log in or register to enroll in the course.")
				}

			})
			// END OF ENROLL 


		})
})