function reloadIt() {
    if (window.location.href.substr(-2) !== "?r") {
        window.location = window.location.href + "?r";
    }
}

	fetch("https://fathomless-cove-53036.herokuapp.com/api/courses", {
		method: "GET",
		headers: ({
			"Content-Type": "application/json",
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data[0]._id)
		console.log(data[1]._id)
		console.log(data[2]._id)


		// -----------


		for(let i = 0; i < data.length; i++){
			let courseContainer = document.querySelector("#courseContainer").innerHTML += 
			`
	          <div class="col mb-4">
	            <div class="card h-100">
	              <img src="../assets/images/course${(i+1)}.jpg" class="card-img-top" alt="...">
	              <div class="card-body">
	                <h5 class="card-title"> ${data[i].name} </h5>
	                <p class = "price"> PHP <span id="coursePrice"> ${data[i].price}.00 </span> <span class="badge badge-warning">43% OFF!</span> <span class = "strike text-muted">PHP 699.00</span> </p>
	                <p class="text-muted smallDes mt-2">${data[i].description}</p>               
	                <a href="./course.html" target="_blank" class="btn btn-block btn-warning mt-3 courseLink" id="course${(i+1)}">
						<i class="fas fa-search spaceIcon"></i>
						Check course details.
					</a>  

					<span class = "toHide">
	                 <button class="btn btn-block btn-dark courseEnroll mt-2" id="enrollContainer">
	                    <i class="fas fa-user-graduate spaceIcon"></i>
	                    Enroll now!
	                </button>
	                </span>

	                <span class = "deactivateSpan"></span>

	              </div>
	            </div>
	          </div>
			`
			console.log(data[i]._id)
			console.log(typeof(data[i]._id))
			console.log(`course${(i+1)}`)
			localStorage.setItem(`course${(i+1)}`, data[i]._id)

			// START OF DEACTIVATION TRIAL

			if(isAdmin == "true"){		

				let toHide = document.getElementsByClassName("toHide")
				for(let i = 0; i < toHide.length; i++){
					toHide[i].innerHTML = ""
				}

				let deactivateSpan = document.getElementsByClassName("deactivateSpan")
				for(let i = 0; i < deactivateSpan.length; i++){
					deactivateSpan[i].innerHTML =
					`
				    <button class="btn btn-block btn-dark mt-2 deactivateBtn">
				    	<i class="fas fa-toggle-off spaceIcon"></i>
		                Deactivate Course
		            </button>
					`
				}

			}

			// END OF DEACTIVATION TRIAL

			let courseLink = document.getElementsByClassName("courseLink");
			for(let i = 0; i < courseLink.length; i++){
			courseLink[i].addEventListener("click", (a) => {
				
				reloadIt();

				localStorage.setItem("imageNo", (i+1))
				let imageNo = window.localStorage.getItem("imageNo")
				console.log(imageNo)

				fetch(`https://fathomless-cove-53036.herokuapp.com/api/courses/${data[i]._id} `, {
					method: "GET",
					headers: ({
						"Content-Type": "application/json",
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					localStorage.setItem("courseId", data._id)
				})


				});
			}
			// --------- start of trial
			let courseEnroll = document.getElementsByClassName("courseEnroll");
			for(let i = 0; i < courseEnroll.length; i++){
			courseEnroll[i].addEventListener("click", (a) => {

				reloadIt();

				// a.preventDefault();

				localStorage.setItem("imageNo", (i+1))
				let imageNo = window.localStorage.getItem("imageNo")
				console.log(imageNo)

				fetch(`https://fathomless-cove-53036.herokuapp.com/api/courses/${data[i]._id} `, {
					method: "GET",
					headers: ({
						"Content-Type": "application/json",
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					localStorage.setItem("courseId", data._id)
					let courseId = localStorage.getItem("courseId")

					// to enroll
					let userId = window.localStorage.getItem('id')
					let isAdmin = window.localStorage.getItem('isAdmin')

					if (isAdmin != "true" && (userId != null || userId != undefined)) {

					fetch("https://fathomless-cove-53036.herokuapp.com/api/users/enroll", {
			            method: "POST",
			            headers: {
			                "Content-Type": "application/json"
			            },
			            body: JSON.stringify({
			                userId: userId,
			                courseId: courseId
			            })
			        })
			        .then(res => {
			            return res.json()
			        })
			        .then(data => {
			            console.log(data)
			            if(data == true){
			            	alert("Congratulations! You have been enrolled.")
			            	window.location.href="./profile.html"
			            } else {
			                alert("Something went wrong. Enrollment unsuccessful.")
			            }
			        })

					} else if(isAdmin == "true"){
						alert("You are an admin.")
					} else {
						alert("Please log in or register to enroll in the course.")
					}
					//  end of to enroll

				})

				});
			}
			// ---------end of trial

		}

	})	


	// START OF INACTIVE courses for admin

	let isAdmin = window.localStorage.getItem('isAdmin')
	if(isAdmin == "true"){
		let inactiveHR = document.querySelector("#inactiveHR").innerHTML =
			`
			<hr class="featurette-divider">
			<h1 class = "text-center pt-3"> Inactive Courses </h1>
			<h6 class = "text-center" id = "noInactive"></h6>
			`

				// start of fetch ACTIVE courses x deactivate


					fetch("https://fathomless-cove-53036.herokuapp.com/api/courses", {
						method: "GET",
						headers: ({
							"Content-Type": "application/json",
						})
					})
					.then(res => res.json())
					.then(data => {
						// console.log(data)
						// console.log(data.length)
						// console.log(data[0]._id)
						// console.log(data[1]._id)
						// console.log(data[2]._id)

						// -----------

						// for(let i = 0; i < data.length; i++){

							// --------- start of trial
							let deactivateBtn = document.getElementsByClassName("deactivateBtn");
							for(let i = 0; i < deactivateBtn.length; i++){
							deactivateBtn[i].addEventListener("click", (a) => {

								reloadIt();

								// a.preventDefault();

								fetch(`https://fathomless-cove-53036.herokuapp.com/api/courses/${data[i]._id} `, {
									method: "PUT",
									headers: ({
										"Content-Type": "application/json",
									})
								})
								.then(res => res.json())
								.then(data => {
									console.log(data)
									if (data === true){
										alert("Course successfully deactivated!")

										function reloadIt() {
										    if (window.location.href.substr(-2) !== "?r") {
										        window.location = window.location.href + "?r";
										    }
										}

										reloadIt();
									} else {
										alert("Something went wrong. Deactivation failed.")
									}
									// data here is either true or false


								})

								});
							}
							// ---------end of trial

						// }

					})	
				// end of fetch active x DEACTIVATE path				


// start of fetch INACTIVE courses x activate


	fetch("https://fathomless-cove-53036.herokuapp.com/api/courses/inactive", {
		method: "GET",
		headers: ({
			"Content-Type": "application/json",
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		console.log(data.length)
		// console.log(data[0]._id)
		// console.log(data[1]._id)
		// console.log(data[2]._id)

		if (data.length == 0){
			let noInactive = document.querySelector("#noInactive").innerHTML = `No inactive courses.`
		}
		// -----------


		for(let i = 0; i < data.length; i++){
			let inactiveContainer = document.querySelector("#inactiveContainer").innerHTML += 
			`
	          <div class="col mb-4">
	            <div class="card h-100">
	              <img src="../assets/images/course${(i+1)}.jpg" class="card-img-top" alt="...">
	              <div class="card-body">
	                <h5 class="card-title"> ${data[i].name} </h5>
	                <p class = "price"> PHP <span id="coursePrice"> ${data[i].price}.00 </span> <span class="badge badge-warning">43% OFF!</span> <span class = "strike text-muted">PHP 699.00</span> </p>
	                <p class="text-muted smallDes mt-2">${data[i].description}</p>               

	                 <button class="btn btn-block btn-warning activateBtn">
	                    <i class="fas fa-toggle-on spaceIcon"></i>
	                    Activate Course
	                </button>

	                 <button class="btn btn-block btn-dark deleteInactiveCourse">
	                    <i class="fas fa-trash-alt spaceIcon"></i>
	                    Delete Course
	                </button>

	              </div>
	            </div>
	          </div>
			`
			console.log(data[i]._id)
			console.log(typeof(data[i]._id))
			console.log(`course${(i+1)}`)

			// --------- start of trial
			let activateBtn = document.getElementsByClassName("activateBtn");
			for(let i = 0; i < activateBtn.length; i++){
			activateBtn[i].addEventListener("click", (a) => {

				reloadIt();
				
				// a.preventDefault();

				fetch(`https://fathomless-cove-53036.herokuapp.com/api/courses/activate/${data[i]._id} `, {
					method: "PUT",
					headers: ({
						"Content-Type": "application/json",
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if (data === true){
						alert("Course successfully activated!")

						function reloadIt() {
						    if (window.location.href.substr(-2) !== "?r") {
						        window.location = window.location.href + "?r";
						    }
						}
						
						reloadIt();
					} else {
						alert("Something went wrong. Activation failed.")
					}
					// data here is either true or false


				})

				});
			}
			// ---------end of trial

			// start of DELETE INACTIVE course

			let deleteInactiveCourse = document.getElementsByClassName("deleteInactiveCourse");
			for(let i = 0; i < deleteInactiveCourse.length; i++){
			deleteInactiveCourse[i].addEventListener("click", (a) => {

				// reloadIt();
				
				// a.preventDefault();

				fetch(`https://fathomless-cove-53036.herokuapp.com/api/courses/${data[i]._id} `, {
					method: "DELETE",
					headers: ({
						"Content-Type": "application/json",
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if (data === true){
						alert("Course successfully deleted!")
						window.location.replace('./deleteCourse.html')

						// function reloadIt() {
						//     if (window.location.href.substr(-2) !== "?r") {
						//         window.location = window.location.href + "?r";
						//     }
						// }
						
						// reloadIt();

					} else {
						alert("Something went wrong. Course not deleted.")
					}

				})

				});
			}

			// end of DELETE INACTIVE course


		}

	})	
// end of fetch inactive x enroll path


	}