let homenavItems1 =  document.querySelector("#homenavSession1") 
let homenavItems2 =  document.querySelector("#homenavSession2") 
let userToken = localStorage.getItem("token")

// console.log(userToken)

if (!userToken) {
//  ----------- FOR HOME PAGE (INDEX.HTML)

	homenavItems1.innerHTML =
		`
			<li class = "nav-item">
				<a href = "./pages/register.html" class = "nav-link"> Register </a>
			</li>
		`

	homenavItems2.innerHTML = 
		`
			<li class = "nav-item">
				<a href = "./pages/login.html" class = "nav-link" style="color: white !important;"> Log in </a>
			</li>	
		`

} else {
//  ----------- FOR HOME PAGE (INDEX.HTML)
		
	homenavItems1.innerHTML = 
		`
			<li class = "nav-item">
				<a href = "./pages/profile.html" class = "nav-link"> Profile </a>
			</li>
		`

	homenavItems2.innerHTML = 
		`
			<li class = "nav-item">
				<a href = "./pages/logout.html" class = "nav-link" style="color: white !important;"> Log out </a>
			</li>	
		`
		
}



