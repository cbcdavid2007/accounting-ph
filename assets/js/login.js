let loginForm = document.querySelector("#loginUser")

loginForm.addEventListener("submit", (e) => {
    e.preventDefault();

    let email = document.querySelector("#userEmail").value
    let password = document.querySelector("#password").value

    if(email == "" || password == ""){
        alert("Please input your email and/or password.")
    } else {
        fetch("https://fathomless-cove-53036.herokuapp.com/api/users/login", {
            method: "POST",
            // POST, because you are going to CREATE ACCESS TOKEN (MATCHING the secret) and CREATE an object returning id, email, isAdmin
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)
            // SAMPLE CONSOLE LOG PAG GUMANA SIYA:
            // i think, si data, yung token
            // {access: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmO…DY3fQ.tS8aEd0yCzE5Gxyh-XhqQzK8qC08hQR_NKG6v9EL4YM"}
            if(data.access){
                // since access is the key, value ni key is - sample - "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmO…DY3fQ.tS8aEd0yCzE5Gxyh-XhqQzK8qC08hQR_NKG6v9EL4YM"
                // ^this can be seen sa taas, yung access: "something"
                localStorage.setItem("token", data.access);
                // ayun na nga, user is able to STORE the token through localstorage. 

                fetch("https://fathomless-cove-53036.herokuapp.com/api/users/details", {
                    // doon sa /details, you're calling a function to get a user from the database.
                    // as long as authorized siya, dun sa 'bearer - data access'
                    headers: {
                        Authorization: `Bearer ${data.access}`
                    }
                })
                .then( res => {
                    return res.json()
                })
                .then(data => {
                    localStorage.setItem("id", data._id)
                    localStorage.setItem("isAdmin", data.isAdmin)
                    // window.location.replace("./profile.html")
                    window.location.replace("./courses.html")
                    // WHAT IF, instead of profile.html, you give them the COURSESSS page na lang?
                })
            } else {
                alert("Something went wrong. Check your input.")
            }
        })
    }
})

// sa routes natin for user.js, naka-POST siya. So dito sa frontend natin, post din siya
// bakit kaya post? Guess ko, kasi we're creating / ADDING authorization token (etc) 
// and since maglogin siya, we want to create an ACCESS TOKEN? na sure na pwede siya mag-access?
// also, baka ginagawan mo na siya ng AUTHENTICATION id sa local storage.

// SAMPLE CONSOLE LOG PAG GUMANA SIYA: {access: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmO…DY3fQ.tS8aEd0yCzE5Gxyh-XhqQzK8qC08hQR_NKG6v9EL4YM"}
// IF DI SIYA GUMANA, CONSOLE LOG: FALSE