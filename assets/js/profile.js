// i think, you can also use:  window.localStorage.getItem('firstName') to display firstname, lastname, etc!!!!
// BUT, MAKE SURE THAT FIRSTNAME, LASTNAME, ETC ARE ALSO DISPLAYED????? KASI FROM SIR, IT'S ONLY ISADMIN AND ID YUNG DISPLAYED EH.  
// so far, here are what's stored:
// localStorage.setItem("token", data.access);
// localStorage.setItem("id", data._id)
// localStorage.setItem("isAdmin", data.isAdmin)
// BAKA, pwede mo naman hanapin si name through ID?
// baka -> get mo si id through the local storage, tapos from id, findById? -> display. get. 
// BAKA MAY NEED KA RIN I-LOCAL STORAGE KAY ADD COURSE -> YUNG ID NIYA?


// ---------------------------start


// function reloadIt() {
//     if (window.location.href.substr(-2) !== "?r") {
//         window.location = window.location.href + "?r";
//     }
// }

function reloadIt() {
    if (window.location.href.substr(-2) !== "?r") {
        window.location = window.location.href + "?r";
    }
}

window.onload = function() {
    if(!window.location.hash) {
        window.location = window.location + '#loaded';
        window.location.reload();
    }
}

window.onload

let userId = window.localStorage.getItem('id')
let isAdmin = window.localStorage.getItem('isAdmin')
let token = window.localStorage.getItem('token')
// let courseId = localStorage.getItem("courseId")
console.log(token)

let profileContainer = document.querySelector("#profileContainer")
// NOTE, you just included it sa baba na lang, tapos nilagyan ng innerhtml!
// note: you didn't target the inner html.

if (!token || token === null) {
	window.location.href="./login.html"
} else if (isAdmin == "true" || userId == null || userId == undefined) {
	window.location.replace('./courses.html')
}  
else {
		fetch("https://fathomless-cove-53036.herokuapp.com/api/users/details", {
			method: "GET",
			headers: ({
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			console.log(data.enrollments)
			console.log(data.enrollments.length)
			// console.log(data.enrollments[0])

			let profileContainer = document.querySelector("#profileContainer").innerHTML =
				`
					<div class="col-md-12 text-center my-5">
	                        <h1 class="mt-5"> ${data.firstName} ${data.lastName} </h1>
	                        <h6>
	                            ${data.email}
	                        </h6>
	                        <h6>
	                            ${data.mobileNo}
	                        </h6>  
	                </div>
				`

			if (data.enrollments.length <= 0) {

				let noEnrollment = document.querySelector("#noEnrollment").innerHTML =
				`
	                No courses enrolled yet.
	                
				`

			} else {

	// START OF FOR LOOPS
			for(let i = 0; i < data.enrollments.length; i++){

				fetch(`https://fathomless-cove-53036.herokuapp.com/api/courses/${data.enrollments[i].courseId} `, {
					method: "GET",
					headers: ({
						"Content-Type": "application/json",
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					console.log(data.name)
					console.log(`Enrolled course ${i+1}`)
					localStorage.setItem(`Enrolled course ${i+1}`, data.name)
					reloadIt();

					// window.localStorage.getItem(`Enrolled course ${i+1}`)
				})

			// for(let i = 0; i < data.enrollments.length; i++){			
				
				let courseContainer = document.querySelector("#courseContainer").innerHTML += 
				`
		          <div class="col mb-4">
		            <div class="card h-100">
		              <div class="card-body">
		                <h5 class="card-title"> ${window.localStorage.getItem(`Enrolled course ${i+1}`)} </h5>
		                <p class="text-muted smallDes my-0"> <strong> Course ID: </strong> ${data.enrollments[i].courseId}</p>               
		                <p class="text-muted smallDes my-0"> <strong> Enrolled on: </strong> ${data.enrollments[i].enrolledOn}</p>               
		                <p class="text-muted smallDes my-0"> <strong> Enrollment status: </strong> ${data.enrollments[i].status}</p>              
		              </div>
		            </div>
		          </div>
				`

				// }



			}
	// END OF FOR LOOPS
		
		}
		// end of else, if enrollment length > 0

		})

}

reloadIt();





// window.location.replace('./profile.html')

// let profileContainer = document.querySelector("#profileContainer").innerHTML += 


			// let classData = data.enrollments.map()
			// console.log(classData)
			// let courseId = data._id

			// let courseName = document.querySelector("#courseName").innerHTML = data.name;
			// let coursePrice = document.querySelector("#coursePrice").innerHTML = data.price;
			// let introDescription = document.querySelector("#introDescription").innerHTML = data.description; 
			// let imageDiv = document.querySelector("#imageDiv").innerHTML = 

			// 	`
			// 	<img src="../assets/images/course${imageNo}.jpg" class="card-img" alt="...">
			// 	`
