let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let email = document.querySelector("#userEmail").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

	// validation (no blanks + passwords match)
	if ((password1 !== "" && password2 !=="") && (password1 === password2) && (mobileNo.length === 11)){
		fetch("https://fathomless-cove-53036.herokuapp.com/api/users/email-exists", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === false){
				// FALSE means EMAIL DOES NOT EXIST! remember si module.exports.emailExists, if TRUE, means, email exists. If false, means, email does not exist.
				fetch("https://fathomless-cove-53036.herokuapp.com/api/users", {
					// no need for / register kasi sa route function mo, si register "/" lang endpoint niya.
					// si /api/users - gives you to register page. No need for /register, kasi yan route niya kay route folder.
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					console.log(data)
					// giving proper response if reg is success
					if (data === true) {
						alert("New Account Has Registered Successfully.")
						window.location.replace("./login.html")
					} else {
						alert("Something went wrong in the registration.")
					}
				})
			}
		})	

	} else {
		alert("Something went wrong. Check your credentials.")
	}
})

// e.preventDefault() is a function na icacancel yung paulit ulit na request, para di paulit ulit kung masend niya man twice.

// fetch request, get url address from route (backend!).

// basically, kaya merong object na method, headers, etc, imbes na kay POSTMAN, dito 
// sa frontend na kasi tayo magcoconnect.\
// yung headers ganyan, nandun siya sa postman. May headers, content-type.
// body: Json, stringify to convert the content into string Json.. usually si email.
// yung after ni FETCH, parang ginagawa na yung ginagawa kay postman.

// .then(res => ) function sets the content type to text/html (from JSON- na naging STRING) then returns response to client.